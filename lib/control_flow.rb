# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  str.each_char do |ch|
    str.delete!(ch) if ch == ch.downcase
  end
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  len = str.length
  if (len % 2 == 0)
    return str[len / 2 - 1] + str[len / 2]
  else
    return str[len / 2]
  end
end

# Return the number of vowels in a string.

def num_vowels(str)
  num = 0
  vowels = ["a", "e", "i", "o", "u"]
  str.each_char.count {|ch| vowels.include?(ch)}
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  res = 1
  (1..num).to_a.each {|a| res = res * a}
  res
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  join = ""
  arr.each_index do |i|
    join += arr[i]
    join += separator unless i == arr.length - 1
  end
  join
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  weirdo = ""
  chars = str.chars
  chars.each_index do |i|
    if i.odd?
      weirdo += chars[i].upcase
    else
      weirdo += chars[i].downcase
    end
  end
  weirdo
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  res = str.split(" ").each {|word| word.reverse! if word.length >= 5}
  res.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  fizzbuzzers = []
    (1..n).each do |int|
      if int % 3 == 0 && int % 5 == 0
        fizzbuzzers << "fizzbuzz"
      elsif int % 5 == 0
        fizzbuzzers << "buzz"
      elsif int % 3 == 0
        fizzbuzzers << "fizz"
      else
        fizzbuzzers << int
      end
    end
    fizzbuzzers
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  res = []
  arr.each {|a| res.unshift(a)}
  res
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num == 1
  (2..num/2).each do |i|
    if num % i == 0
      return false
    end
  end
  true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  factors = []
  (1..num).each do |i|
    if num % i == 0
      factors << i
    end
  end
  factors
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  prime_factors = []
    factors(num).each do |factor|
      prime_factors << factor if prime?(factor)
    end
    prime_factors
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  evens = []
  odds = []
  arr.each do |int|
    if int.even?
      evens << int
    else
      odds << int
    end
  end

  if evens.length > 1
    return odds[0]
  end
  evens[0]
end
